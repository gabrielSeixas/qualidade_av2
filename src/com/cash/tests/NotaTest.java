import com.cash.enums.ValorNota;
import com.cash.models.Nota;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by gabriel on 31/03/16.
 */
public class NotaTest {

    @Test
    public void getValor() throws Exception {
        Nota nota = new Nota(ValorNota.CEM);

        assertEquals(nota.getValor(), new Integer(100));
    }
}
import com.cash.dao.NotaDAOImpl;
import com.cash.enums.ValorNota;
import com.cash.models.Nota;

import static org.junit.Assert.*;

/**
 * Created by gabriel on 31/03/16.
 */
public class NotaDAOImplTest {

    @org.junit.Before
    public void setUp() throws Exception {
        System.out.println("starting test...");
    }

    @org.junit.Test
    public void adicionar() throws Exception {
        NotaDAOImpl dao = new NotaDAOImpl();
        dao.adicionar(new Nota(ValorNota.CEM));

        assertEquals(1, dao.listar().size());
        assertEquals(1, dao.getListaValores().size());
    }

    @org.junit.Test
    public void pegar() throws Exception {
        NotaDAOImpl dao = new NotaDAOImpl();
        dao.adicionar(new Nota(ValorNota.DEZ));
        dao.adicionar(new Nota(ValorNota.CINQUENTA));

        dao.retirar(ValorNota.CINQUENTA);

        assertEquals(1, dao.listar().size());
        assertEquals(2, dao.getListaValores().size());
    }
}
package com.cash.dao;

import com.cash.models.Nota;
import com.cash.enums.ValorNota;

import java.util.*;

/**
 * Created by gabriel on 30/03/16.
 */
public class NotaDAOImpl implements NotaDAO {

    private List<Nota> notas;
    private Map<ValorNota, Integer> listaDeValores;

    public NotaDAOImpl() {
        this.notas = new LinkedList<>();
        this.listaDeValores = new TreeMap<>();

        this.listaDeValores.put(ValorNota.CEM, 0);
        this.listaDeValores.put(ValorNota.CINQUENTA, 0);
        this.listaDeValores.put(ValorNota.VINTE, 0);
        this.listaDeValores.put(ValorNota.DEZ, 0);
        this.listaDeValores.put(ValorNota.CINCO, 0);
    }

    public List<Nota> adicionar(Nota nota) {
        this.notas.add(nota);
        this.editaListaDeValores(nota.getValor(), true);
        this.ordenaNotas();
        return this.notas;
    }

    public List<Nota> retirar(ValorNota valor) {
        Nota nota = new Nota(valor);
        this.removeNota(nota);
        this.editaListaDeValores(nota.getValor(), false);
        return this.notas;
    }

    public List<Nota> listar() {
        return this.notas;
    }

    public Map<ValorNota, Integer> getListaValores() {
        return this.listaDeValores;
    }

    public void removeQtdNotas(ValorNota valor, Integer quantidade) {
        Integer qtdDeNotasRestante = this.listaDeValores.get(valor) - quantidade;
        this.listaDeValores.put(valor, qtdDeNotasRestante);
    }

    public Integer qtdNotasPorValor(ValorNota valor) {
        return this.listaDeValores.get(valor);
    }

    // Private methods

    private List<Nota> removeNota(Nota notaProcurada) {
        Iterator<Nota> iterator = this.notas.iterator();
        while (iterator.hasNext()) {
            Nota n = iterator.next();
            if (notaProcurada.equals(n)) {
                iterator.remove();
                return this.notas;
            }
        }
        return null;
    }

    private void editaListaDeValores(ValorNota valor, Boolean removeOuAdiciona) {
        Integer quantidade = this.listaDeValores.get(valor);
        quantidade = removeOuAdiciona ? quantidade + 1 : quantidade - 1;
        this.listaDeValores.replace(valor, quantidade);
    }

    private void ordenaNotas() {
        Collections.sort(this.notas, (Nota n1, Nota n2) ->
            n1.getValor().compareTo(n2.getValor())
        );
    }
}

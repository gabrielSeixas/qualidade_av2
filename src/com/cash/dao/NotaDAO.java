package com.cash.dao;

import com.cash.enums.ValorNota;
import com.cash.models.Nota;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gabriel on 31/03/16.
 */
public interface NotaDAO {
    List<Nota> adicionar(Nota nota);
    List<Nota> retirar(ValorNota valor);
    List<Nota> listar();
    Map<ValorNota, Integer> getListaValores();
}

package com.cash.models;

import com.cash.enums.ValorNota;

/**
 * Created by gabriel on 30/03/16.
 */
public class Nota {

    private ValorNota valor;

    public Nota(ValorNota valor) {
        this.valor = valor;
    }

    public ValorNota getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return this.valor.toString();
    }


    public boolean equals(Nota nota) {
        return this.getValor() == nota.getValor();
    }

    @Override
    public int hashCode() {
        return this.getValor().getValor();
    }
}

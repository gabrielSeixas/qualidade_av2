package com.cash.enums;

/**
 * Created by gabriel on 30/03/16.
 */
public enum ValorNota {
    CEM(100),
    CINQUENTA(50),
    VINTE(20),
    DEZ(10),
    CINCO(5),
    DOIS(2);

    private Integer valor;

    ValorNota(Integer valor) {
        this.valor = valor;
    }

    public Integer getValor() {
        return this.valor;
    }

    @Override
    public String toString() {
        return this.valor.toString();
    }
}

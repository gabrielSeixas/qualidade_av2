package com.cash.main;

import com.cash.dao.NotaDAOImpl;
import com.cash.enums.ValorNota;
import com.cash.models.Nota;
import com.sun.org.apache.bcel.internal.generic.FLOAD;
import com.sun.xml.internal.ws.api.message.ExceptionHasMessage;

/**
 * Created by gabriel on 31/03/16.
 */
public class ControleSaldo {

    private NotaDAOImpl dao;
    private Integer saldo;

    public ControleSaldo(NotaDAOImpl dao) {
        this.dao = dao;
        this.saldo = 0;
    }

    public void addSaldo(ValorNota valor, Integer quantidade) {
        for (int i = 0; i < quantidade; i++) {
            this.dao.adicionar(new Nota(valor));
            this.saldo += valor.getValor();
        }
    }

    public Integer removeSaldo(Integer quantia) throws ArithmeticException {
        Integer valorRetirado = quantia;
        if (quantia <= this.saldo) {
            for (ValorNota valorNota : ValorNota.values()) {
                Integer quantiaDaNota = this.dao.getListaValores().get(valorNota);
                if (quantia >= valorNota.getValor()) {
                    quantia = this.retiraNotas(quantia, valorNota);
                }
                if (quantia == 0) {
                    break;
                }
            }
            this.saldo -= valorRetirado;
        } else {
            return null;
        }
        return this.saldo;
    }

    public Integer getSaldo() {
        return this.saldo;
    }

    public NotaDAOImpl getNotaDao() {
        return this.dao;
    }

    private Integer retiraNotas(Integer quantia, ValorNota valor) throws ArithmeticException {
        float valorSemArredondar = (quantia / valor.getValor());

        if (valorSemArredondar < 1) {
            throw new ArithmeticException();
        }

        Integer qtdNota = (int) Math.floor(valorSemArredondar);

        if (this.dao.getListaValores().get(valor) < qtdNota) {
            qtdNota = this.dao.getListaValores().get(valor);
        }

        this.dao.removeQtdNotas(valor, qtdNota);
        return (quantia - qtdNota*valor.getValor());
    }
}

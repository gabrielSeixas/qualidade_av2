package com.cash.main;

import com.cash.enums.ValorNota;

import java.util.Scanner;

/**
 * Created by gabriel on 31/03/16.
 */
public class TelaCaixa {

    private Scanner inputs;
    private CaixaEletronico caixaEletronico;
    private static TelaCaixa tela;


    private static final int REPOR = 1;
    private static final int SACAR = 2;
    private static final int SALDO = 3;
    private static final int FIM = 4;


    private TelaCaixa() {
        this.inputs = new Scanner(System.in);
        this.caixaEletronico = new CaixaEletronico();
    }

    public static TelaCaixa getInstance() {
        if (tela == null) {
            tela = new TelaCaixa();
        }
        return tela;
    }

    public void iniciaTela() {
        Integer opcao;
        boolean executar = true;
        while (executar) {
            opcao = this.mostraMenu();
            executar = this.realizaOperacoes(opcao);
        }
    }

    private Integer mostraMenu() {
        System.out.println("\n---------------------------------------");
        System.out.println("Caixa Eletrônico - Menu de Opções");
        System.out.println("---------------------------------------");
        System.out.println("1- Repor");
        System.out.println("2- Sacar");
        System.out.println("3- Consultar saldo");
        System.out.println("4- Fim");
        System.out.print("Opção: ");
        return inputs.nextInt();
    }

    private Boolean realizaOperacoes(Integer opcao) {
        Boolean executar = true;
        switch(opcao) {
            case REPOR:
                this.repor();
                break;

            case SACAR:
                this.sacar();
                break;

            case SALDO:
                this.verificaSaldo();
                break;

            case FIM:
                System.out.print("Tchau. Até a proxima.");
                executar = false;
                break;
            default:
                System.out.println("Opção inválida!");
                break;
        }
        return executar;
    }

    private void repor() {
        System.out.println("\n---------------------------------------");
        System.out.println("Caixa Eletrônico - Reposição de notas");
        System.out.println("---------------------------------------");
        System.out.print("Informa quantidade de notas de 100 reais: ");
        this.caixaEletronico.depositar(ValorNota.CEM, this.inputs.nextInt());

        System.out.print("Informa quantidade de notas de 50 reais: ");
        this.caixaEletronico.depositar(ValorNota.CINQUENTA, this.inputs.nextInt());

        System.out.print("Informa quantidade de notas de 20 reais: ");
        this.caixaEletronico.depositar(ValorNota.VINTE, this.inputs.nextInt());

        System.out.print("Informa quantidade de notas de 10 reais: ");
        this.caixaEletronico.depositar(ValorNota.DEZ , this.inputs.nextInt());

        System.out.print("Informa quantidade de notas de 5 reais: ");
        this.caixaEletronico.depositar(ValorNota.CINCO, this.inputs.nextInt());
    }

    private void sacar() {
        System.out.println("\n---------------------------------------");
        System.out.println("Caixa Eletrônico - Saque");
        System.out.println("---------------------------------------");
        System.out.print("Quantia: ");
        Integer quantiaSaque = this.inputs.nextInt();

        this.manipulaSaque(quantiaSaque);

        this.caixaEletronico.listaQuatidadeDeCadaNota();
    }

    private void verificaSaldo() {
        System.out.println(caixaEletronico.getSaldo());
        caixaEletronico.listaQuatidadeDeCadaNota();
    }

    private void manipulaSaque(Integer quantiaSaque) {
        try {
            Integer valorRestante = this.caixaEletronico.sacar(quantiaSaque);

            if (valorRestante != null) {
                System.out.println("Saldo " + valorRestante);
            } else {
                System.out.println("Valor insuficiente para saque");
            }
        } catch(NullPointerException | ArithmeticException e) {
            System.out.println("Quantidade de notas insuficiente para saque!");
        }
    }

}

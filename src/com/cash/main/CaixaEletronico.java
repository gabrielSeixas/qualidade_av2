package com.cash.main;

import com.cash.dao.NotaDAOImpl;
import com.cash.enums.ValorNota;

/**
 * Created by gabriel on 31/03/16.
 */
public class CaixaEletronico {

    private static CaixaEletronico caixa = null;
    private ControleSaldo controleSaldo;
    private NotaDAOImpl dao;

    public CaixaEletronico() {
        this.dao = new NotaDAOImpl();
        this.controleSaldo = new ControleSaldo(this.dao);
    }

    public void depositar(ValorNota valor, Integer quantidade) {
        this.controleSaldo.addSaldo(valor, quantidade);
    }

    public Integer sacar(Integer valor) throws ArithmeticException {
        Integer saldo = this.controleSaldo.removeSaldo(valor);
        return saldo == null ? null : this.controleSaldo.getSaldo();
    }

    public Integer getSaldo() {
        return this.controleSaldo.getSaldo();
    }

    public void listaQuatidadeDeCadaNota() {
        this.dao.getListaValores().forEach( (key, value) -> {
            System.out.println("Nota de " + key + " tem " + value + " notas");
        });
    }
}
